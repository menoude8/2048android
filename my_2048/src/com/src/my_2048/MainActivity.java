package com.src.my_2048;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends ActionBarActivity {
	MapView gameGridModel = new MapView(4,4);
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        UIGame view = (UIGame)(findViewById(R.id.game_view));
        if (id == R.id.action_newgame) {
        	Log.i("New Game", "Start New Game");
            List<MapView.Action> animList = new ArrayList<MapView.Action>();
            gameGridModel.doNewGame(animList);
            view.startAnim(animList);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

    
    @Override
    protected void onResume() {
        super.onResume();
        UIGame view = (UIGame)(findViewById(R.id.game_view));
        view.setModel(gameGridModel);
        view.invalidate();
        
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        gameGridModel.restoreState(preferences);
        if (preferences.getBoolean("NEW_GAME", true) == true)
            gameGridModel.doNewGame(null);

    }
    
    @Override
    protected void onPause() {
        super.onPause();
        
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        
        editor.putBoolean("NEW_GAME", false);
        gameGridModel.saveState(editor);
        
        editor.commit();
    }
    
}
