package com.src.my_2048;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import android.content.SharedPreferences;

public class MapView {
    Random rand = new Random();
	private static final int MAX_RANG_TO_ADD = 2;
    public enum DIRECTION { UP, DOWN, LEFT, RIGHT, NONE }
    Tile[][] map;
    int sizeX;
    int sizeY;
    int currentScore = 0;
    int highScore = 0;
    public enum GAMESTATE { PLAY, GAMEOVER, WIN };
    GAMESTATE gameState = GAMESTATE.PLAY;
    int foundedSpace;
    int lastRangForMerge;
    
    public int getScore() { 
    	return currentScore;
    }
    
    private class Tile {
        public int rang;
        public int[] canMove = new int[4];
        public boolean[] canMerge = new boolean[4];

        public int getCanMove(DIRECTION dir) {
            return canMove[getDir(dir)];
        }

        public void setCanMove(DIRECTION dir, int value) {
            canMove[getDir(dir)] = value;
        }

        public boolean getCanMerge(DIRECTION dir) {
            return canMerge[getDir(dir)];
        }

        public void setCanMerge(DIRECTION dir, boolean value) {
            canMerge[getDir(dir)] = value;
        }

        private int getDir(DIRECTION dir) {
            if (dir == DIRECTION.RIGHT)
            	return 0;
            else if (dir == DIRECTION.LEFT)
            	return 1;
            else if (dir == DIRECTION.DOWN)
            	return 2;
            else if (dir == DIRECTION.UP)
            	return 3;
            return -1;
        }
    }

    public class Action {
        public int type;
        public int oldX;
        public int oldY;
        public int newX;
        public int newY;
        public int rang;
        public static final int NOTHING = 0;
        public static final int MOVE = 1;
        public static final int CREATE = 2;
        public static final int MERGE = 3;

        public Action(int type, int rang, int oldX, int oldY, int newX, int newY) {
            this.type = type;
            this.rang = rang;
            this.oldX = oldX;
            this.oldY = oldY;
            this.newX = newX;
            this.newY = newY;
        }
    }
    
    public int getHighscore() {
    	return highScore;
    }
    
    public void updateHighscore() { 
        if (currentScore > highScore) { 
        	highScore = currentScore;
        } 
    }

    public MapView(int squareSizeX, int squareSizeY) {
        sizeX = squareSizeX;
        sizeY = squareSizeY;
        map = createData(sizeX, sizeY);
    }

    Tile[][] createData(int width, int height) {
        Tile[][] newData = new Tile[width][];
        for (int x = 0; x < sizeX; x++) {
            newData[x] = new Tile[height];
            for (int y = 0; y < sizeY; y++)
            	newData[x][y] = new Tile();
        }
        return newData;
    }

    public void doNewGame(List<Action> actionHistory) {
        updateHighscore();
        for(int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++)
            	map[x][y].rang = 0;
        }
        gameState = GAMESTATE.PLAY;
        addNewTileToRandomCell(actionHistory);
        addNewTileToRandomCell(actionHistory);
        updateMovingAbilities();
        currentScore = 0;
    }

    public int get(int x, int y) {
        return map[x][y].rang;
    }

    public void doMove(DIRECTION dir, List<Action> actionHistory) {
    	boolean canDoMove = false;
        List<int[]> increaseRang = new ArrayList<int[]>();
        Tile[][] newData = createData(sizeX, sizeY);
        
    	updateState();
        if (gameState != GAMESTATE.PLAY)
        	return;
        for(int x = 0; x < sizeX; x++) {
            for(int y = 0; y < sizeY; y++) {
            	int newX = x;
            	int newY = y;
                
            	if (map[x][y].rang == 0)
                	continue;
                if (dir == DIRECTION.UP)
                	newY -= map[x][y].getCanMove(DIRECTION.UP);
                else if (dir == DIRECTION.DOWN)
                	newY += map[x][y].getCanMove(DIRECTION.DOWN);
                else if (dir == DIRECTION.RIGHT)
                	newX += map[x][y].getCanMove(DIRECTION.RIGHT);
                else if (dir == DIRECTION.LEFT)
                	newX -= map[x][y].getCanMove(DIRECTION.LEFT);
                newData[newX][newY] = map[x][y];
                if (map[x][y].getCanMerge(dir)) {
                    increaseRang.add(new int[] { 
                    		newX, newY, (map[x][y].rang+1) 
                    });
                }
                if (actionHistory != null)
                    actionHistory.add(new Action(Action.MOVE, map[x][y].rang, x, y, newX, newY));               
                if (y != newY || x != newX)
                    canDoMove = true;
            }
        }
        
        for (int[] tile : increaseRang) {
            currentScore += Math.pow(2,tile[2]);
            
            if (tile[2] == 11)
                gameState = GAMESTATE.WIN;
            newData[tile[0]][tile[1]].rang = tile[2];
            if (actionHistory != null)
                actionHistory.add( new Action(Action.CREATE, tile[2], tile[0], tile[1], tile[0], tile[1]) );
        }
        
        if (canDoMove) {
        	map = newData;
            addNewTileToRandomCell(actionHistory);
            updateMovingAbilities();
        }
    }

    public boolean isAbleToMove(int x, int y, DIRECTION dir) {
        return (map[x][y].getCanMove(dir) != 0 || map[x][y].getCanMerge(dir));
    }

    public GAMESTATE getGameState() {
        return gameState;
    }

    void addNewTileToRandomCell(List<Action> actionHistory) {
    	int x;
    	int y;
    	
    	if (gameState != GAMESTATE.PLAY) 
        	return;
    	x = rand.nextInt(sizeX);
        y = rand.nextInt(sizeY);
    	while (map[x][y].rang != 0) {
    		x = rand.nextInt(sizeX);
            y = rand.nextInt(sizeY);
    	}
    	map[x][y].rang = rand.nextInt(MAX_RANG_TO_ADD) + 1;
        if (actionHistory != null) {
            actionHistory.add(new Action(Action.CREATE, map[x][y].rang, x, y, x, y));
        }
    }

    private void updateTile(int x, int y, DIRECTION dir) {
        if (map[x][y].rang == 0)
            foundedSpace++;
        else {
        	map[x][y].setCanMove(dir, foundedSpace);
            if (map[x][y].rang == lastRangForMerge) {
            	map[x][y].setCanMerge(dir, true);
                lastRangForMerge = 0;
                foundedSpace++;
                map[x][y].setCanMove(dir, foundedSpace);
            } 
            else {
                lastRangForMerge = map[x][y].rang;
                map[x][y].setCanMerge(dir, false);
            }
        }
    }

    private void updateMovingAbilities() {
    	for(int y = 0; y < sizeY; y++) {
            foundedSpace = 0;
            lastRangForMerge = 0;
            for (int x = sizeX - 1; x >= 0; x--) {
                updateTile(x,y,DIRECTION.RIGHT);
            }
        }
        for(int y = 0; y < sizeY; y++) {
            foundedSpace = 0;
            lastRangForMerge = 0;
            for (int x = 0; x < sizeX; x++) {
                updateTile(x,y,DIRECTION.LEFT);
            }
        }
        for(int x = 0; x < sizeX; x++) {
            foundedSpace = 0;
            lastRangForMerge = 0;
            for (int y = 0; y < sizeY; y++) {
                updateTile(x,y,DIRECTION.UP);
            }
        }
        for(int x = 0; x < sizeX; x++) {
            foundedSpace = 0;
            lastRangForMerge = 0;
            for (int y = sizeY - 1; y >= 0; y--) {
                updateTile(x,y,DIRECTION.DOWN);
            }
        }
        updateState();
    }
    
    void updateState() {
        boolean noMove = true;
        gameState = GAMESTATE.PLAY;
        
        for(int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                if (map[x][y].rang == 11)
                    gameState = GAMESTATE.WIN;
                if (noMove)
                    for(int i = 0; i < map[x][y].canMove.length; i++)
                        if (map[x][y].canMove[i] != 0)
                            noMove = false;
            }
        }
        if (noMove && gameState != GAMESTATE.WIN)
            gameState = GAMESTATE.GAMEOVER;
    }
    
    public void saveState(SharedPreferences.Editor bundle) {
        bundle.putInt("GRID_WIDTH", sizeX);
        bundle.putInt("GRID_HEIGHT", sizeY);
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                bundle.putInt("GRID_DATA_" + x + "_" + y, map[x][y].rang);
            }
        }
        bundle.putInt("CURRENT_SCORE", currentScore);
        bundle.putInt("HIGH_SCORE", highScore);
    }
    
    public void restoreState(SharedPreferences bundle) {
        sizeX = bundle.getInt("GRID_WIDTH", 4);
        sizeY = bundle.getInt("GRID_HEIGHT", 4);
        map = createData(sizeX, sizeY);
        
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
            	map[x][y].rang = bundle.getInt("GRID_DATA_" + x + "_" + y, 0); 
            }
        }
        currentScore = bundle.getInt("CURRENT_SCORE", 0);
        highScore = bundle.getInt("HIGH_SCORE", 0);
        updateMovingAbilities();
    }
}
