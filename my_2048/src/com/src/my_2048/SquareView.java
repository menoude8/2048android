package com.src.my_2048;

import android.graphics.Canvas;
import android.graphics.RectF;

public interface SquareView {
	void draw(Canvas c, RectF rect, int rang, float howCloseToDissapear);
}
