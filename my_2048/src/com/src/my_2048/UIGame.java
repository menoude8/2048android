package com.src.my_2048;

import java.util.ArrayList;
import java.util.List;

import com.src.my_2048.MapView.DIRECTION;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class UIGame extends View {
	int backgroundColor = Color.parseColor("#297CCF");
	int mainSquareColor = Color.parseColor("#297CCF");
	int mainSquareRoundAngles = 5;
	int gridSquareColor = Color.parseColor("#ffffff");
	int gridSpacing = 12;
	int gridMargins;    
	Paint mainSquarePaint = new Paint();
	Paint gridPaint = new Paint();
	SquareView squareView;
	RectF mainSquare;
	RectF leftTopSquare = new RectF();
	int animStartTime = (int) System.currentTimeMillis();
	final static int ANIM_DURATION = 150;
    final static int ANIM_CREATE_DELAY = 80;
    final static int ANIM_CREATE_DURATION = 200;
    final static int ANIM_FRAMEDELAY = 33; // 1000/30
    public List<MapView.Action> animList = null;
    RectF gridTempSquare = new RectF();
    Point animLastOffset = new Point();
    DIRECTION lastDir = DIRECTION.NONE;
    Paint resultPaint = new Paint();
    int animFadeGameover = 0;
    Rect resultSquare = new Rect();
    boolean animStop = false;
    Handler animHandler = new Handler();    
    Runnable animationInvalidator = new Runnable() {
        @Override
        public void run() {
            invalidate();
            if ( !(animStop == true && animList == null) ) 
                animHandler.postDelayed(this, ANIM_FRAMEDELAY);
        }
    };
    Point gestureCenter = new Point();
    public Point gestOffset = new Point();
    Point fancyGridOffset = new Point();
    int offsetSize = 0;
    private static final int MINIMAL_RANGE = 30;
    MapView model;
    
	public UIGame(Context context, AttributeSet attrs) {
		super(context, attrs);
        mainSquarePaint = new Paint();
        mainSquarePaint.setColor(mainSquareColor);
        mainSquarePaint.setAntiAlias(true);
        gridPaint = new Paint();
        gridPaint.setColor( gridSquareColor );
        gridPaint.setAntiAlias(true);
        squareView = new SquareViewByDefault(context);
	}
	
	public void setModel(MapView model) {
        this.model = model;
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldw, int oldh) {
        super.onSizeChanged(width, height, oldw, oldh);        
        if (model == null)
        	return;
        if (width <= height)
            mainSquare = new RectF(0, height/2-width/2, width, height/2+width/2);
        else
        	mainSquare = new RectF(width/2-height/2, 0, width/2+height/2, height);
        int squareSizeX = (int)((mainSquare.width() - gridMargins*2 - (model.sizeX-1)*gridSpacing)/model.sizeX);
        int squareSizeY = (int)((mainSquare.height() - gridMargins*2 - (model.sizeY-1)*gridSpacing)/model.sizeY);
        leftTopSquare.set(mainSquare.left + gridMargins, mainSquare.top + gridMargins, mainSquare.left + gridMargins + squareSizeX, mainSquare.top + gridMargins + squareSizeY);
    }
    
    boolean anim(Canvas canvas) { 
        double time = ((int) System.currentTimeMillis() - animStartTime)/(double)(ANIM_DURATION);
        double createTime = ((int) System.currentTimeMillis() - animStartTime - ANIM_CREATE_DELAY) / (double)(ANIM_CREATE_DELAY + ANIM_CREATE_DURATION);
        if (createTime < 0.0)
        	createTime = 0.0;
        if (createTime > 1.0) {
            animList = null;
            return false;
        }
        if (time > 1.0)
        	time = 1.0;
        time = Math.pow(time, 0.33);

        for (int zIndex = 0; zIndex < 3; zIndex++) {
            for (MapView.Action iter : animList) {
                double posX = iter.oldX + (iter.newX - iter.oldX) * time;
                double posY = iter.oldY + (iter.newY - iter.oldY) * time;
                gridTempSquare.offsetTo( (float)(leftTopSquare.left + posX*(leftTopSquare.width() + gridSpacing)),
                                       (float)(leftTopSquare.top +  posY*(leftTopSquare.height() + gridSpacing)));
                if ( (iter.oldX != iter.newX) || (iter.oldY != iter.newY)) {
                	gridTempSquare.offset( (float)(animLastOffset.x*(1-time)), (float)(animLastOffset.y*(1-time)) );
                }

                if (iter.type == MapView.Action.MOVE && zIndex == 0) {
                    squareView.draw(canvas, gridTempSquare, iter.rang, 0.0f);
                }
                else if (iter.type == MapView.Action.MERGE && zIndex == 1) {
                	squareView.draw(canvas, gridTempSquare, iter.rang+1, (float)(1-time));
                }
                else if (iter.type == MapView.Action.CREATE && zIndex == 2) {
                    if (createTime != 0)
                    	squareView.draw(canvas, gridTempSquare, iter.rang, (float)createTime);
                }
            }
        }
        return true;
    }
    
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        
        if (model == null)
        	return;
        canvas.drawColor(backgroundColor);
        canvas.drawRoundRect( mainSquare,
                mainSquareRoundAngles, mainSquareRoundAngles,
                mainSquarePaint);
        gridTempSquare.set(leftTopSquare);
        updateFancyGridOffset();
                
        for(int zIndex = 0; zIndex < 3; zIndex++) {
            if (zIndex > 0 && animList != null) {
                if (anim(canvas)) 
                    break;
            }

            for (int i = 0; i < model.sizeX; i++) {
                for (int j = 0; j < model.sizeY; j++) {
                	gridTempSquare.offsetTo( 
                            (float)(leftTopSquare.left + i*(leftTopSquare.width() - 5 + gridSpacing)),
                            (float)(leftTopSquare.top +  j*(leftTopSquare.height() - 5 + gridSpacing)));
                    if (zIndex == 0) {
                        canvas.drawRoundRect( gridTempSquare,
                                mainSquareRoundAngles, mainSquareRoundAngles,
                                gridPaint);
                    }
                    if (lastDir != DIRECTION.NONE) {
                        if (zIndex == 1 && !model.isAbleToMove(i, j, lastDir )) {
                            squareView.draw(canvas, gridTempSquare, model.get(i,j), 0.0f);
                        }
                        if (zIndex == 2 && model.isAbleToMove(i, j, lastDir)) {
                        	gridTempSquare.offset( fancyGridOffset.x, fancyGridOffset.y );
                            squareView.draw(canvas, gridTempSquare, model.get(i,j), 0.0f);
                        }
                    } 
                    else
                    	squareView.draw(canvas, gridTempSquare, model.get(i,j), 0.0f);                        
                }
            }
        }
        
        if (model.getGameState() != MapView.GAMESTATE.PLAY) {
            resultPaint.setColor(backgroundColor);
            if (animFadeGameover > 240) {
                animFadeGameover = 240;
                animStop = true;
            } else {
                animFadeGameover += 4;
            }
            
            if (animFadeGameover > 10) {
	            resultPaint.setAlpha(animFadeGameover);
	            canvas.drawRect(mainSquare, resultPaint);
	            
	            resultPaint.setColor(Color.WHITE);
	            resultPaint.setAlpha(animFadeGameover);
	            resultPaint.setTextAlign(Align.CENTER);
	            
	            resultPaint.setTextSize(30);
	            resultPaint.getTextBounds("A", 0, 1, resultSquare);
	            int fontHeight = (int)(resultSquare.height()*1.5);
	            
	            if (model.getGameState() == MapView.GAMESTATE.GAMEOVER)
	                canvas.drawText("Vous avez perdu !", mainSquare.centerX(), mainSquare.centerY()+fontHeight*-1, resultPaint);
	            else
	                canvas.drawText("Félicitations vous avez gagné.", mainSquare.centerX(), mainSquare.centerY()+fontHeight*-1, resultPaint);
	            
	            canvas.drawText("Ton record : " + model.getScore(), mainSquare.centerX(), mainSquare.centerY()+fontHeight*1, resultPaint);
	            if (model.getScore() > model.getHighscore()) {
	                canvas.drawText("Nouveau score : ", mainSquare.centerX(), mainSquare.centerY()+fontHeight*2, resultPaint);
	                canvas.drawText("Dernier score : " + model.getHighscore(), mainSquare.centerX(), mainSquare.centerY()+fontHeight*3, resultPaint);
	            } else {
	                canvas.drawText("Meilleur score : " + model.getHighscore(), mainSquare.centerX(), mainSquare.centerY()+fontHeight*2, resultPaint);
	            }
            }
        } 
        else
            animFadeGameover = 0;
        resultPaint.setColor(Color.WHITE);
        resultPaint.setTextAlign(Align.LEFT);
        resultPaint.setTextSize(30);
        resultPaint.getTextBounds("A", 0, 1, resultSquare);
        int fontHeight = (int)(resultSquare.height()*1.5);
        canvas.drawText("Meileur score : " + model.getHighscore(), 30, 30+fontHeight*1, resultPaint);
        canvas.drawText("Ton score : " + model.getScore(), 30, 30+fontHeight*2, resultPaint);
    }
    
    public void startAnim(List<MapView.Action> animList) {
        this.animList = animList;
        animStartTime = (int) System.currentTimeMillis();
        animStop = false;
        animHandler.postDelayed(animationInvalidator, ANIM_FRAMEDELAY);
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        if (model == null)
        	return false;
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            if (model.getGameState() != MapView.GAMESTATE.PLAY) {
                startAnim(null);
            } 
            else {
                gestureCenter.set((int) ev.getX(), (int) ev.getY());
                gestOffset.set(0,0);
                fancyGridOffset = new Point(gestureCenter);
                lastDir = DIRECTION.NONE;
                animList = null;
                animStop = true;
            }
            return true;
        }
        else if (ev.getAction() == MotionEvent.ACTION_MOVE) {
            if (model.getGameState() == MapView.GAMESTATE.PLAY) {
                gestOffset.set(
                        (int)ev.getX() - gestureCenter.x,
                        (int)ev.getY() - gestureCenter.y );
    
                invalidate();
            }
            return true;
        }
        else if (ev.getAction() == MotionEvent.ACTION_UP) {
            if (offsetSize > MINIMAL_RANGE) {
                List<MapView.Action> anims = new ArrayList<MapView.Action>();
                model.doMove(lastDir, anims);
                animLastOffset.set(fancyGridOffset.x, fancyGridOffset.y);
                startAnim(anims);
            }
            gestOffset.set(0, 0);
            lastDir = DIRECTION.NONE;
            invalidate();
            return true;
        }
        return super.onTouchEvent(ev);
    }


    private void updateFancyGridOffset() {
    	int offsetX = gestOffset.x;
        int offsetY = gestOffset.y;

        if (Math.abs(offsetX - offsetY) > MINIMAL_RANGE) {
            if ( Math.abs(gestOffset.x) > Math.abs(gestOffset.y) ) {
                if (gestOffset.x > 0)
                	lastDir = DIRECTION.RIGHT;
                else
                	lastDir = DIRECTION.LEFT;
            } 
            else {
                if (gestOffset.y > 0)
                	lastDir = DIRECTION.DOWN;
                else
                	lastDir = DIRECTION.UP;
            }

        }
        
        if (lastDir == DIRECTION.DOWN || lastDir == DIRECTION.UP) {
            offsetX = 0;
            if (offsetY > 0)
            	lastDir = DIRECTION.DOWN;
            else
            	lastDir = DIRECTION.UP;
        } 
        else {
            offsetY = 0;
            if (offsetX > 0) lastDir = DIRECTION.RIGHT;
            else             lastDir = DIRECTION.LEFT;
        }

        int maxX = (int)((leftTopSquare.width() - 5 + gridMargins)*0.68);
        int maxY = (int)((leftTopSquare.height() - 5 + gridMargins)*0.68);
        if (Math.abs(offsetX) > maxX) 
            offsetX = maxX * (int)Math.signum(offsetX);
        if (Math.abs(offsetY) > maxY)
            offsetY = maxY * (int)Math.signum(offsetY);
        offsetSize = Math.max(Math.abs(offsetX), Math.abs(offsetY));
        if (offsetSize < MINIMAL_RANGE) {
            offsetX = (int)(MINIMAL_RANGE * Math.pow(offsetX/(double)MINIMAL_RANGE, 3)); 
            offsetY = (int)(MINIMAL_RANGE * Math.pow(offsetY/(double)MINIMAL_RANGE, 3));
            offsetSize = Math.abs(offsetX + offsetY);
        }
        fancyGridOffset.set(offsetX, offsetY);
    }
}
