package com.src.my_2048;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.RectF;

import com.src.my_2048.R;

public class SquareViewByDefault implements SquareView{
    Paint tilePaint = new Paint();
    RectF tempRect = new RectF();
    Rect bounds = new Rect();
    int radiusAngle;
    static final int MAX_SIZE_INCREASE = 15;
    int fontSize;
    int desiredFontSize_forBounds = 0;
    
    int[] tileColorArr = new int[]{
            R.color.squareColor0, R.color.squareColor1, R.color.squareColor2, R.color.squareColor3,
            R.color.squareColor4, R.color.squareColor5, R.color.squareColor6, R.color.squareColor7,
            R.color.squareColor8, R.color.squareColor9, R.color.squareColor10, R.color.squareColor11
    };
    int[] tileTextColorArr = new int[]{
            R.color.textColor0, R.color.textColor1, R.color.textColor2, R.color.textColor3,
            R.color.textColor4, R.color.textColor5, R.color.textColor6, R.color.textColor7,
            R.color.textColor8, R.color.textColor9, R.color.textColor10, R.color.textColor11
    };	
	
	@Override
	public void draw(Canvas c, RectF rect, int rang, float sizeMax) {
		int realRang = rang;
        if (rang == 0)
        	return;
        
        if (rang > 11)
        	rang = 11;

        tilePaint.setAntiAlias(true);
        tilePaint.setColor(tileColorArr[rang]);

        tempRect.set(rect);
        if (sizeMax != 0) {
        	int newSize = (int)(Math.pow((1 - sizeMax),3) * MAX_SIZE_INCREASE);
            tempRect.inset(-1*newSize, -1*newSize);
        }
        c.drawRoundRect(tempRect, radiusAngle, radiusAngle, tilePaint);
        String text = "" + (int)(Math.pow(2,realRang));

        tilePaint.setColor(tileTextColorArr[rang]);
        tilePaint.setTextAlign(Align.CENTER);
        if ( desiredFontSize_forBounds != tempRect.width()*0.8 ) {
            final float testTextSize = 48f;
            tilePaint.setTextSize(testTextSize);
            tilePaint.getTextBounds("2048", 0, 4, bounds);
            fontSize = (int) ((testTextSize * (tempRect.width()*0.8)) / bounds.width());
            desiredFontSize_forBounds = (int) (tempRect.width()*0.8);
        }
        tilePaint.setTextSize(fontSize);
        
        tilePaint.getTextBounds(text, 0, text.length(), bounds);
        float centerX = tempRect.centerX();// - bounds.width()/2;
        float centerY = tempRect.centerY() + bounds.height()/2;

        c.drawText(text, centerX, centerY, tilePaint);
	}
  
    public SquareViewByDefault(Context context) {
    	this.radiusAngle = 2;
    	for (int i=0; i < tileColorArr.length; i++) {
    		tileColorArr[i] = context.getResources().getColor(tileColorArr[i]);
    		tileTextColorArr[i] = context.getResources().getColor(tileTextColorArr[i]);
    	}
    }
}
